<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AccueilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // récupération des produit où
        // la catégorie est agale à masque
        // masque = 1 en id de catégorie
        $les_masques = DB:: table('produits')
                    ->where('categories_prod','=', '1')    
                    ->get();

        // récupération des produit où
        // la catégorie est agale à plantes
        // plantes = 2 en id de catégorie
        $les_plantes = DB:: table('produits')
        ->where('categories_prod','=', '2')    
        ->get();

        // récupération des produit où
        // la catégorie est agale à tissus
        // tissus = 3 en id de catégorie
        $les_tissus = DB:: table('produits')
                    ->where('categories_prod','=', '3')    
                    ->get();

        // récupération des produit où
        // la catégorie est agale à tableau
        // tableau = 4 en id de catégorie
        $les_tableaux = DB:: table('produits')
        ->where('categories_prod','=', '4')    
        ->get();

                
          
        return view ('accueil', compact('les_masques','les_plantes','les_tissus','les_tableaux' ));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
