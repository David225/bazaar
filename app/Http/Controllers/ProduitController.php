<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // on rempli les_produits avec tous les
        // articles de la bdd
        $les_produits = DB:: table('produits')
                     ->join('categories', 'produits.categories_prod', 'categories.categorie_id')
                     ->get();
                    
        // Le nom de catégorie est général pour tous les articles
        $categoryName = 'Voici tous nos articles:';
        return view ('produits', compact('les_produits','categoryName' ));
    }

    public function indexCategory($category)
    {
        // on rempli les_produits selon la category envoyée 
        // avec les articles de la bdd correspondants
        $les_produits = DB::table('produits')
                    ->where('produits.categories_prod', '=', $category )
                    ->get();

        //si la category entré n'existe pas on retoune la page vide
        if (count($les_produits) == 0)
        {
            $categoryName = 'Aucun article trouvé dans cette catégorie !';
            return view ('produits', compact('les_produits','categoryName' ));
        }

        // on récupère le nom de catégorie en bdd
       $GetCategoryName = DB:: table('categories') 
                        ->select('nom_categorie')
                        ->where('categorie_id','=', $category)
                        ->get();
             
        $categoryName =  $GetCategoryName[0]->nom_categorie;
        return view ('produits', compact('les_produits','categoryName' ));
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function produitunique(int $id)
    {
        //
        $un_produit = DB::table('produits')
                    ->where('produits.id_produit', '=', $id )
                    ->get();
          if(count($un_produit) == 0 ){
              return view('error.404');

          }else{
              return view('singleproduct',['un_produit' => $un_produit[0]]);
          }         

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
