<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->increments('id_produit');
            $table->string('nom');
            $table->integer('prix');
            $table->integer('note');
            $table->text('description');
            $table->integer('categories_prod');
            $table->timestamps();
        });

       /* Schema::table('produits', function($table) {
            $table->foreign('categories_prod')->references('categorie_id')->on('categories_produits');
          });
          */
         
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produits');
    }
}
