<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categorie=new \App\Categories();
        $categorie->nom_categorie = "masque";
        $categorie->save();

        $categorie=new \App\Categories();
        $categorie->nom_categorie = "plante";
        $categorie->save();


        $categorie=new \App\Categories();
        $categorie->nom_categorie = "tissus";
        $categorie->save();

        $categorie=new \App\Categories();
        $categorie->nom_categorie = "tableau";
        $categorie->save();
    }
}
