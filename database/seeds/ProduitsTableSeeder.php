<?php

use Illuminate\Database\Seeder;

class ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $produit= new \App\produits();
        $produit->nom = "masque krou";
        $produit->prix = 120000;
        $produit->note =1;
        $produit->description ="Masque porter pendant les fetes traditionelles dans le groupe Krou en Cote d'ivoire";
        $produit->categories_prod = 1;
        $produit->images ="masque4.png";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "masque mande ";
        $produit->prix = 190000;
        $produit->note =2;
        $produit->description ="Masque porter pendant les fetes traditionelles dans le groupe Mande en Cote d'ivoire";
        $produit->categories_prod = 1;
        $produit->images ="masque5.png";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "masque gour";
        $produit->prix = 150000;
        $produit->note =3;
        $produit->description ="Masque porter pendant les fetes traditionelles dans le groupe Gour en Cote d'ivoire";
        $produit->categories_prod = 1;
        $produit->images ="masque6.png";
        $produit->save();
        
        $produit= new \App\produits();
        $produit->nom = "masque akan ";
        $produit->prix = 160000;
        $produit->note =3;
        $produit->description ="Masque porter pendant les fetes traditionelles dans groupe Akan en Cote d'ivoire";
        $produit->categories_prod = 1;
        $produit->images ="masque1.png";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "le berger";
        $produit->prix = 50000;
        $produit->note =1;
        $produit->description ="Reprsentation d'un berger dans le village Senoufo en Cote d'ivoire";
        $produit->categories_prod = 4;
        $produit->images ="tableau2.jpg";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "les milles tambours";
        $produit->prix = 150000;
        $produit->note =3;
        $produit->description =" Representation des Tambours traditionelles dans chez les Gouro en Cote d'ivoire";
        $produit->categories_prod = 4;
        $produit->images ="tableau3.jpg";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "la Kora";
        $produit->prix = 140000;
        $produit->note =4;
        $produit->description ="Representation d'une dame jouant de la Kora ";
        $produit->categories_prod = 4;
        $produit->images ="tableau1.jpg";
        $produit->save();


        $produit= new \App\produits();
        $produit->nom = "mini plante";
        $produit->prix = 50000;
        $produit->note =3;
        $produit->description ="Une plante pour decorer votre maison";
        $produit->categories_prod = 2;
        $produit->images ="plante2.jpg";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "plante a pandule";
        $produit->prix = 10000;
        $produit->note =3;
        $produit->description ="Emporter cette plante pour et placer la comme bon vous semble";
        $produit->categories_prod = 2;
        $produit->images ="plante3.jpg";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "feuille parfumer ";
        $produit->prix = 20000;
        $produit->note =4;
        $produit->description ="fleurs parfumées, feuilles aromatiques, écorces et résines aux fragrances puissantes, graines odorantes";
        $produit->categories_prod = 2;
        $produit->images ="plante.jpg";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "plante";
        $produit->prix = 150000;
        $produit->note =3;
        $produit->description ="Ce sont des plantes qui attirent les énergies positives, selon les principes du Feng Shui";
        $produit->categories_prod = 2;
        $produit->images ="plante1.jpg";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "tissu hollandais";
        $produit->prix = 250000;
        $produit->note =3;
        $produit->description =" matière extrêmement répandue dans notre quotidien et un tissu facile à coudre et couper idéal pour les couturières débutantes";
        $produit->categories_prod = 3;
        $produit->images ="tissus1.jpg";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "tissu wax";
        $produit->prix = 100000;
        $produit->note =4;
        $produit->description ="Un tissu facile à coudre et couper idéal pour les couturières et le mieux pour la femme africaine";
        $produit->categories_prod = 3;
        $produit->images ="tissus2.jpg";
        $produit->save();

        $produit= new \App\produits();
        $produit->nom = "tissu bogolan";
        $produit->prix = 170000;
        $produit->note =5;
        $produit->description ="Tissus porter pendant les fetes traditionelles  en Cote d'ivoire";
        $produit->categories_prod = 3;
        $produit->images ="tissus3.jpg";
        $produit->save();

        

        
        
        
    }
}
