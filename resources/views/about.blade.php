@extends('layouts.layout')
@section('content')


    <!-- ***** Main Banner Area Start ***** -->
    <div class="page-heading about-page-heading" id="top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner-content">
                        <h2>A notre propos </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** About Area Starts ***** -->
    <div class="about-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-image">
                        <img src="{{url('assets/images/panier5.png')}}" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content">
                        <h4>ce que nous faisons</h4>
                        <span>Notre mission est d’améliorer la vente d'objets   africains en proposant aux consommateurs des services en ligne innovants, pratiques et abordables tout en soutenant le développement de la culture africaine.</span>

                        <p>Notre plate-forme rassemble un marché qui relie les acheteurs, qui facilite les transactions de nos clients sur notre marché.</p>
                    
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** About Area Ends ***** -->

    <!-- ***** Our Team Area Starts ***** -->
    <section class="our-team">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>Notre equipe</h2>
                        <span>.</span>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="team-item">
                        <div class="thumb" id="carre">
                            <div class="hover-effect">
                                
                            </div>
                            <img src="{{url('assets/images/personnage.png')}}">
                        </div>
                        <div class="down-content">
                            <h4>Dian Aboa</h4>
                            <span>Responsable Administratif</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="team-item">
                        <div class="thumb"id="carre">
                            <div class="hover-effect">
                                
                            </div>
                            <img src="{{url('assets/images/personnage.png')}}">
                        </div>
                        <div class="down-content">
                            <h4>David Agouassi</h4>
                            <span>Developpeur Junior</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="team-item">
                        <div class="thumb"id="carre">
                            <div class="hover-effect">
                               
                            </div>
                            <img src="{{url('assets/images/personnage.png')}}">
                        </div>
                        <div class="down-content">
                            <h4>Thomas Choyer</h4>
                            <span>Gerant</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Our Team Area Ends ***** -->

    <!-- ***** Services Area Starts ***** -->
    <section class="our-services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>Nos services</h2>
                        <span>L'equipe Tomora vous offre des services bien diversifier en development Web</span>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="service-item">
                        <h4>Conception de site vitrine</h4>
                        <p>Un site qui présente des produits et services d'un organisme en ligne. Afin de simplement attirer l'attention et susciter l'intérêt des utilisateurs d'Internet.</p>
                        <img src="{{url('assets/images/vitrine.jpg')}}" alt="">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="service-item">
                        <h4>Conception de site Ecommerce </h4>
                        <p>Le e-commerce n'est que l'achat et la vente de produits et de services sur Internet.</p>
                        <img src="{{url('assets/images/ecommerce.jpg')}}" alt="">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="service-item">
                        <h4>Conception de site blog</h4>
                        <p>Le site blog est utilisés pour publier périodiquement et régulièrement des articles personnels.</p>
                        <img src="{{url('assets/images/blog.jpg')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Services Area Ends ***** -->

    @endsection