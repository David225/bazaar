
@extends('layouts.layout')
@section('content')
    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner" id="top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content" id="forme">
                        <div class="thumb">
                            <div class="inner-content">
                                <h4>Vous etes au BAZAAR</h4>
                                <span>Genial ! Ici vous trouverez <br> l'ensemble de vos objets décoratifs purement africains.</span>
                                <div class="main-border-button" id="bout">
                                    <a href="{{url('/produits')}}">C'est parti!</a>
                                </div>
                            </div>
                            <img src="{{url('assets/images/bogoln.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="right-first-image">
                                    <div class="thumb" id="carre">
                                        <div class="inner-content">
                                            <h4>Masque</h4>
                                            <span>Découvrez nos plus beau masque</span>
                                        </div>
                                        <div class="hover-content" >
                                            <div class="inner">
                                                   <div class="main-border-button">
                                                   <a href="{{url('/produits/1')}}">voir plus</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="{{url('assets/images/masque.jpg')}}" >
                                    </div>
                                </div>
                            </div>  
                            <div class="col-lg-6" >
                                <div class="right-first-image">
                                    <div class="thumb" id="carre">
                                        <div class="inner-content">
                                            <h4>Tableau</h4>
                                            <span>L'arts Africains ses plus dessins</span>
                                        </div>
                                        <div class="hover-content">
                                            <div class="inner">
                                                 <div class="main-border-button">
                                                    <a href="{{url('/produits/4')}}">voir plus</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="{{url('assets/images/tableau.jpg')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="right-first-image">
                                    <div class="thumb" id="carre">
                                        <div class="inner-content">
                                            <h4>Plantes</h4>
                                            <span>Nos plus belles plantes</span>
                                        </div>
                                        <div class="hover-content">
                                            <div class="inner">
                            
                                                <div class="main-border-button">
                                                    <a href="{{url('/produits/2')}}">Voir plus</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="{{url('assets/images/plante.jpg')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="right-first-image">
                                    <div class="thumb" id="carre">
                                        <div class="inner-content">
                                            <h4>Tissus</h4>
                                            <span>Des Tissus faites par des Tisserand</span>
                                        </div>
                                        <div class="hover-content">
                                            <div class="inner">
                                                <div class="main-border-button">
                                                    <a href="{{url('/produits/3')}}">Voir plus</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="{{url('assets/images/tissu.jpg')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

    
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** Men Area Starts ***** -->
    <section class="section" id="men">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-heading">
                        <h2>Masque</h2>
                        <span>Offrez vous des Masque porter dans le temps par des genies Africains</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="men-item-carousel">
                        <div class="owl-men-item owl-carousel">
                         @foreach($les_masques as $masque) 
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content" >
                                        <ul>
                                            <li><a href="{{url('singleproduct/'.$masque->id_produit)}}"><i class="fa fa-eye"></i></a></li>
                                            <li><a href="{{url('singleproduct/'.$masque->id_produit)}}"><i class="fa fa-star"></i></a></li>
                                            <li><a href="{{url('singleproduct/'.$masque->id_produit)}}"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                    </div>
                                    <img src="{{ asset('assets/images/BD/'.$masque->images)}}" alt=""id="carre">
                                </div>
                                <div class="down-content">
                                    <h4>{{$masque->nom}}</h4>
                                    <span>{{$masque->prix}}Franc CFA</span>
                                    <ul class="stars">
                                        @for ($i = 0; $i < $masque->note; $i++)
                                                                <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-diamond-fill" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                                            </svg></i></li>
                                        @endfor
                                        @for ($i = 5; $i > $masque->note; $i--)
                                        <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="lightgrey" class="bi bi-diamond-empty" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                                            </svg></i></li>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                         @endforeach
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
   
    <!-- ***** MASQUE Area Ends ***** -->

    <!-- ***** TABLEAU Area Starts ***** -->

    <section class="section" id="women">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-heading">
                        <h2>Tableau</h2>
                        <span>Des Tableaux qui racontre l'histoire des artistes Africains.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="women-item-carousel" >
                        <div class="owl-women-item owl-carousel">
                        @foreach($les_tableaux as $tableau) 
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="{{url('singleproduct/'.$tableau->id_produit)}}"><i class="fa fa-eye"></i></a></li>
                                            <li><a href="{{url('singleproduct/'.$tableau->id_produit)}}"><i class="fa fa-star"></i></a></li>
                                            <li><a href="{{url('singleproduct/'.$tableau->id_produit)}}"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                    </div>
                                    <img src="{{ asset('assets/images/BD/'.$tableau->images)}}" alt="" id="carre">
                                </div>
                                <div class="down-content">
                                    <h4>{{$tableau->nom}}</h4>
                                    <span>{{$tableau->prix}}Franc CFA</span>
                                    <ul class="stars">
                                        @for ($i = 0; $i < $tableau->note; $i++)
                                        <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-diamond-fill" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                                            </svg></i></li>
                                        @endfor
                                        @for ($i = 5; $i > $tableau->note; $i--)
                                        <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="lightgrey" class="bi bi-diamond-empty" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                                            </svg></i></li>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                            @endforeach
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    <!-- ***** TABLEAU Area Ends ***** -->

    <!-- ***** TISSUS Area Starts ***** -->
    <section class="section" id="kids">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-heading">
                        <h2>Tissus</h2>
                        <span>Vous avez le choix des plus tissus vendu en afrique.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="kid-item-carousel">
                        <div class="owl-kid-item owl-carousel">
                          @foreach($les_tissus as $tissu) 
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="{{url('singleproduct/'.$tissu->id_produit)}}"><i class="fa fa-eye"></i></a></li>
                                            <li><a href="{{url('singleproduct/'.$tissu->id_produit)}}"><i class="fa fa-star"></i></a></li>
                                            <li><a href="{{url('singleproduct/'.$tissu->id_produit)}}"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                    </div>
                                    <img src="{{ asset('assets/images/BD/'.$tissu->images)}}" alt="" class="diamond">
                                </div>
                                <div class="down-content">
                                    <h4>{{$tissu->nom}}</h4>
                                    <span>{{$tissu->prix}}Franc CFA</span>
                                    <ul class="stars">
                                        @for ($i = 0; $i < $tissu->note; $i++)
                                                                <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-diamond-fill" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                                            </svg></i></li>
                                        @endfor
                                        @for ($i = 5; $i > $tissu->note; $i--)
                                        <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="lightgrey" class="bi bi-diamond-empty" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                                            </svg></i></li>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                          @endforeach
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <!-- ***** Explore Area Starts ***** -->
    <section class="section" id="explore">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content">
                        <h2>Decouvrez notre source d'inspiration</h2>
                        <span>Ce site Ecommerce à été construit selon les bases du style africain </span>
                        
                        <p>Bazaar a été créé de sorte a modéliser le style africain at travers une page web, cest un site commercial simple d'utilisation pour une expérience client optimal. ses images en forme de losange font la particularité de ce site, cette forme était attribuer aux images pour donner un aperçu du tissu bogolan,  d'où la forme du losange est la plus perçue.
                             Des motifs tirés du bogolan que vous retouverez sur l'ensemble des pages du site. </p>
                        
                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="leather">
                                    <h4>Du bogolan</h4>
                                    <span><!-- ***** ajout text ***** --></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="first-image">
                                    <img src="{{url('assets/images/africa.jpg')}}" alt="" class="diamond">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="second-image">
                                    <img src="{{url('assets/images/image2.jpg')}}" alt="" class="diamond">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="types">
                                    <h4> Des losanges</h4>
                                    <span><!-- ***** ajout text ***** --></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- ***** Explore Area Ends ***** -->
    
    @endsection
