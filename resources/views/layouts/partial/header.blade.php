 <!-- ***** Header Area Start ***** -->
 <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="{{url('/')}}" class="logo">
                           <span class="log"> BAZAAR <span>
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="{{url('/')}}">Accueil</a></li>
                            
                            <li class="submenu">
                                <a href="{{route('voir_produits')}}">Nos produits</a>
                                <ul>
                                    <li><a href="{{url('/produits/1')}}">Masque</a></li>
                                    <li><a href="{{url('/produits/4')}}">Tableau</a></li>
                                    <li><a href="{{url('/produits/3')}}">tissus</a></li>
                                    <li><a href="{{url('/produits/2')}}">Plante</a></li>
                                </ul>
                            </li>
                            <li class="scroll-to-section"><a href="{{url('/about')}}">A propos</a></li>
                            <li class="scroll-to-section"><a href="{{url('/contacts')}}">Contact</a></li>
                            
                            <li class="scroll-to-section"><a href="{{url('/#explore')}}" >Notre style</a></li>
                        </ul>        
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->