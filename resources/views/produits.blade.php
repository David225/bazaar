@extends('layouts.layout')
@section('content')

   
    <!-- ***** Main Banner Area Start ***** -->
    <div class="page-heading" id="top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner-content">
                        <h2>Decouvrez nos produits</h2>
                        <span> <!-- peut inscrire du text--></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
   


    <!-- ***** Products Area Starts ***** -->
    <section class="section" id="products">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>{{$categoryName}}</h2>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
              @foreach($les_produits as $produit)
                <div class="col-lg-4">
                    <div class="item">
                        <div class="thumb">
                            <div class="hover-content">
                                <ul>        <!-- singleproduct/{{$produit->id_produit}} -->
                                    <li><a href="{{url('singleproduct/'.$produit->id_produit)}}"><i class="fa fa-eye"></i></a></li>
                                    <li><a href="{{url('singleproduct/'.$produit->id_produit)}}"><i class="fa fa-star"></i></a></li>
                                    <li><a href="{{url('singleproduct/'.$produit->id_produit)}}"><i class="fa fa-shopping-cart"></i></a></li>
                                </ul>            
                            </div>
                            <img src="{{ asset('assets/images/BD/'.$produit->images)}}" alt="" id="carre">
                        </div>
                        <div class="down-content">
                            <h4>{{$produit->nom}}</h4>
                            <span>{{$produit->prix}}</span>
                            <ul class="stars">
                                @for ($i = 0; $i < $produit->note; $i++)
                                                        <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-diamond-fill" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                                    </svg></i></li>
                                @endfor
                                @for ($i = 5; $i > $produit->note; $i--)
                                <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="lightgrey" class="bi bi-diamond-empty" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                                    </svg></i></li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>
              @endforeach
                
            </div>
        </div>
    </section>
    <!-- ***** Products Area Ends ***** -->
    
  
    @endsection