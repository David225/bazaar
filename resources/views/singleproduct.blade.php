@extends('layouts.layout')
@section('content')
    <!-- ***** Main Banner Area Start ***** -->
    <div class="page-heading" id="top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner-content">
                        <h2>{{$un_produit->nom}}</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Main Banner Area End ***** -->


    <!-- ***** Product Area Starts ***** -->
    <section class="section" id="product">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                <div class="left-images">
                    <img src="{{ asset('assets/images/BD/'.$un_produit->images)}}" alt="">
                    
                </div>
            </div>
            <div class="col-lg-4">
                <div class="right-content">
                    <h4 >{{$un_produit->nom}}</h4>
                    <hr id="souligner">
                    <span class="price">{{$un_produit->prix}}</span>
                    <ul class="stars">
                        @for ($i = 0; $i < $un_produit->note; $i++)
                                                <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-diamond-fill" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                            </svg></i></li>
                        @endfor
                        @for ($i = 5; $i > $un_produit->note; $i--)
                        <li><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="lightgrey" class="bi bi-diamond-empty" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"/>
                            </svg></i></li>
                        @endfor
                    </ul>
                    <span>{{$un_produit->description}}</span>
                    
                    <div class="quantity-content">
                        <div class="left-content">
                            <h6>Nombre d'objets</h6>
                        </div>
                        <div class="right-content">
                            <div class="quantity buttons_added">
                                <input type="button" value="-" class="minus"><input type="number" step="1" min="1" max="50" name="quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="" inputmode=""><input type="button" value="+" class="plus">
                            </div>
                        </div>
                    </div>
                    <div class="total">
                        <h4>{{$un_produit->prix}} Franc cfa</h4>
                        <div class="main-border-button"><a href="#">Acheter</a></div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    <!-- ***** Product Area Ends ***** -->
    @endsection